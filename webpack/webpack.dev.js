const common = require('./webpack.common');
const { merge } = require('webpack-merge');

const devConfig = merge(
  common,

  {
    mode: 'development',
  }
);

module.exports = devConfig;
