const common = require('./webpack.common');
const { merge } = require('webpack-merge');

const TerserPlugin = require('terser-webpack-plugin');
const SpeedMeasurePlugin = require('speed-measure-webpack-plugin');

const smp = new SpeedMeasurePlugin({
  disable: !process.env.MEASURE,
})

const prodConfig = merge(common, {
  mode: 'production',

  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin()
    ],
  },
});

module.exports = smp.wrap(prodConfig);
