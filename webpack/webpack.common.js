const { merge } = require('webpack-merge');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const WindiCSSWebpackPlugin = require('windicss-webpack-plugin');
const scripts = require('./modules/scripts');
const styles = require('./modules/styles');
const directories = require('./modules/directories');

const commonConfig = merge(
  {
    entry: {
      app: directories.source + 'main.js',
    },

    output: {
      path: directories.distribution,
      filename: '[name].js',
    },

    target: 'web',

    resolve: {
      extensions: ['*', '.js', '.jsx', '.ts', '.tsx'],
      alias: {
        '@': `${directories.source}`,
        '@components': `${directories.source}/components`,
        '@styles': `${directories.source}/styles`,
      },
    },

    plugins: [
      new CleanWebpackPlugin(),
      new WindiCSSWebpackPlugin()
    ],
  },
  scripts(),
  styles()
);

module.exports = commonConfig;
