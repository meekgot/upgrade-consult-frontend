export const colors = {
  'shark': '#1C2529',
  'sandy-beach': '#FFF0C8',
  'dandelion': '#FDD05A',
  'outer-space': '#374246',
};
