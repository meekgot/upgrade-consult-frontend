export const screens = {
  ty: '500px',
  sm: '768px',
  md: '1024px',
  lg: '1280px',
  xl: '1440px',
  xll: '1920px',
};
