import { defineConfig } from 'windicss/helpers';
import { colors } from './windi/colors';
import { screens } from './windi/screens';

export default defineConfig({
  darkMode: false,
  theme: {
    extend: {
      fontFamily: {
        jost: 'Jost',
      },
      screens,
      colors,
      extend: {},
    },
  },
  scan: {
    dirs: ['src'],
    exclude: ['node_modules', '.git', 'public/**/*', '*.template.html', 'index.html'],
    include: [],
  },
  transformCSS: 'pre',
});
