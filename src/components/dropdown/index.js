const dropdown = (el) => {
  console.log(el);
  const toggleButton = el.querySelector('.dropdown__toggle');

  const toggleDropdown = () => {
    el.classList.toggle('dropdown--open')
  }

  if (toggleButton) {
    toggleButton.addEventListener('click', toggleDropdown)
  }
}

export const initDropdown = () => {
  const dropdowns = document.querySelectorAll('.dropdown')

  dropdowns.forEach((el) => {
    dropdown(el)
  })
}
