import VanillaScrollspy from 'vanillajs-scrollspy';

const smoothScroll = (target) => {
  const scrollspy = VanillaScrollspy(target, 1000);
  scrollspy.init();
}

export const initSmoothScroll = () => {
  const elements = document.querySelectorAll('.smooth-scroll');
  elements.forEach(el => {
    smoothScroll(el);
  });
};
