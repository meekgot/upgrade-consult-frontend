const MenuMob = () => {
  const body = document.body;
  const button = document.querySelector('.header__burger');
  const closeButton = document.querySelector('.menu-mob__close');
  const menu = document.querySelector('.menu-mob');
  const showClass = 'menu-mob--show';

  const openMenu = () => {
    menu.classList.add(showClass);
    body.style.overflow = 'hidden';
  };

  const closeMenu = () => {
    menu.classList.remove(showClass);
    body.style.overflow = 'auto';
  };

  if (button && closeButton) {
    button.addEventListener('click', openMenu);
    closeButton.addEventListener('click', closeMenu);
  }
};

export const initMenuMob = () => {
  MenuMob();
};
