import Swiper, { Navigation } from 'swiper';
import 'swiper/css';
import 'swiper/css/navigation';

const source = 'slider';

const Slider = (node) => {
  // console.log(node.getAttribute('config'));

  new Swiper(node, {
    slidesPerView: 1.2,
    spaceBetween: 30,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    breakpoints: {
      768: {
        slidesPerView: 3,
        spaceBetween: 30,
      },
    },
    modules: [Navigation],
  });
};

export const initSlider = () => {
  const sliders = document.querySelectorAll(`.${source}`);

  sliders.forEach((item) => {
    Slider(item);
  });
};
