// vendor
import '@styles/vendor/normalize.scss';

// base
import '@styles/settings/base.scss';

// components
import '@styles/components/title.scss';
import '@styles/components/button.scss';
import '@styles/components/page.scss';
import '@styles/components/menu.scss';
import '@styles/components/menu-mob.scss';
import '@styles/components/header.scss';
import '@styles/components/portrait.scss';
import '@styles/components/emoji-list.scss';
import '@styles/components/motivation-list.scss';
import '@styles/components/bonus.scss';
import '@styles/components/program.scss';
import '@styles/components/recive.scss';
import '@styles/components/bonuses-list.scss';
import '@styles/components/dropdown.scss';
import '@styles/components/author.scss';
import '@styles/components/tariffs.scss';
import '@styles/components/form.scss';
import '@styles/components/registration.scss';
import '@styles/components/socials.scss';
import '@styles/components/review-card.scss';
import '@styles/components/footer.scss';

// pages
import '@styles/pages/webinar.scss';
import '@styles/pages/course.scss';