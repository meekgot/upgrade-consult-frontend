import { initSlider } from '@components/slider';
import { initMenuMob } from '@components/menu-mob';
import { initSmoothScroll } from '@components/smooth-scroll';
import { initDropdown } from '@components/dropdown';
import '@styles/index.js';

const app = {
  init: () => {
    initSlider();
    initMenuMob();
    initSmoothScroll();
    initDropdown();
  },
};

document.addEventListener('DOMContentLoaded', app.init());
